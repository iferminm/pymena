from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailsearch import index


class BlogIndexPage(Page):
    intro = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full')
    ]


class BlogPost(Page):
    date = models.DateField()
    last_updated = models.DateField()
    body = RichTextField()

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('last_updated'),
        FieldPanel('body', classname='full')
    ]

